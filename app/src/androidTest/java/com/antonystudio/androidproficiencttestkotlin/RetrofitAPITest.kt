package com.antonystudio.androidproficiencttestkotlin

import android.content.Intent
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.antonystudio.androidproficiencttestkotlin.activity.MainActivity
import com.antonystudio.androidproficiencttestkotlin.network.RetrofitClientInstance
import com.squareup.okhttp.mockwebserver.MockResponse
import com.squareup.okhttp.mockwebserver.MockWebServer
import junit.framework.TestCase
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.Timeout

class RetrofitAPITest : TestCase() {
    @Rule
    var mActivityRule: ActivityTestRule<MainActivity> = ActivityTestRule<MainActivity>(
        MainActivity::class.java, true, false
    )
    private var server: MockWebServer? = null

    @Rule
    var globalTimeout = Timeout.seconds(30)

    @Before
    @Throws(Exception::class)
    public override fun setUp() {
        super.setUp()
        server = MockWebServer()
        server!!.start()
        //   injectInstrumentation(getInstrumentation());
        RetrofitClientInstance.BASE_URL = server!!.url("/").toString()
    }

    @Test
    @Throws(Exception::class)
    fun testQuoteIsShown() {
        val fileName = "200_ok_response.json"
        server!!.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(
                    RestServiceTestHelper.getStringFromFile(
                        InstrumentationRegistry.getInstrumentation()
                            .context, fileName
                    )
                )
        )
        val intent = Intent()
        mActivityRule.launchActivity(intent)
    }

    @After
    @Throws(Exception::class)
    public override fun tearDown() {
        server!!.shutdown()
    }
}