package com.antonystudio.androidproficiencttestkotlin.utils

import android.app.AlertDialog
import android.content.Context
import android.net.ConnectivityManager
import android.view.ContextThemeWrapper
import com.antonystudio.androidproficiencttestkotlin.R

object CommonUtils {
    //checking network connection
    fun isNetConnected(context: Context): Boolean {
        val cm =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting
    }

    fun getAlert(ctx: Context, message: String?): AlertDialog {
        val builder = AlertDialog.Builder(
            ContextThemeWrapper(
                ctx,
                R.style.Theme_AppCompat_Light_Dialog_Alert
            )
        )
            .setMessage(message).setPositiveButton(
                ctx.getString(R.string.ok)
            ) { dialogInterface, _ -> dialogInterface.cancel() }
        return builder.create()
    }
}