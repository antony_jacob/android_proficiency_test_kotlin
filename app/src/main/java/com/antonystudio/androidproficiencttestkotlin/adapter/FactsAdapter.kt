package com.antonystudio.androidproficiencttestkotlin.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.antonystudio.androidproficiencttestkotlin.R
import com.antonystudio.androidproficiencttestkotlin.response.Rows
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

class FactsAdapter(private val ctx: Context) : RecyclerView.Adapter<FactsAdapter.ViewHolder>() {
    private var boolTitle = false
    private var boolDescription = false
    private var boolImageHref = false
    private var item: View? = null
    private var rowsList : List<Rows> = listOf()
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        item = LayoutInflater.from(ctx).inflate(R.layout.listview_model_item, parent, false)
        return ViewHolder(item)
    }

    fun setRowList(rowLIst:List<Rows>){
        this.rowsList = rowLIst
    }

    override fun onBindViewHolder(holder: ViewHolder,position: Int) {

        val title:String = rowsList[position].title
        if (title != null && title!!.length > 0) {
            holder.mTitle.text = rowsList[position].title
            holder.mTitle.visibility = View.VISIBLE
            boolTitle = true
        } else {
            holder.mTitle.visibility = View.GONE
            boolTitle = false
        }
        val desc:String = rowsList[position].description
        if (desc != null && desc!!.length > 0 ) {
            holder.mDescription.text = rowsList[position].description
            holder.mDescription.visibility = View.VISIBLE
            boolDescription = true
              } else {
                  holder.mDescription.visibility = View.GONE
                  boolDescription = false
              }
        var imageUrl: String = rowsList[position].imageHref
       if (imageUrl != null && imageUrl!!.length > 0) {
            imageUrl = imageUrl.replace("http://", "https://")
            Picasso.get()
                .load(imageUrl)
                .into(holder.mImageHrefImageVIew, object : Callback {
                    override fun onSuccess() {}
                    override fun onError(e: Exception) {
                        holder.mImageHrefImageVIew.setBackgroundResource(R.drawable.error_loading)
                    }
                })
            holder.mImageHrefImageVIew.visibility = View.VISIBLE
            boolImageHref = true
        } else {
            holder.mImageHrefImageVIew.visibility = View.GONE
            boolImageHref = false
        }
         if (!boolTitle && !boolDescription && !boolImageHref) {
            holder.layoutMain.visibility = View.GONE
        } else if (!boolDescription && !boolImageHref) {
            holder.layoutDescMain.visibility = View.GONE
        } else {
            holder.layoutMain.visibility = View.VISIBLE
            holder.layoutDescMain.visibility = View.VISIBLE
        }
    }

    override fun getItemCount(): Int {
        return rowsList.size
    }

    class ViewHolder(view: View?) : RecyclerView.ViewHolder(view!!) {
        lateinit var mTitle: TextView
        lateinit var mDescription: TextView
        lateinit var mImageHrefImageVIew: ImageView
        lateinit var layoutDescMain: RelativeLayout
        lateinit var layoutMain: RelativeLayout

        init {
            if (view != null) {
                mTitle = view.findViewById(R.id.textview_title)
                mDescription = view.findViewById(R.id.textview_description)
                mImageHrefImageVIew = view.findViewById(R.id.fact_imageview)
                layoutDescMain = view.findViewById(R.id.layout_desc_image)
                layoutMain = view.findViewById(R.id.linear_layout_main)
            }
        }
    }

}