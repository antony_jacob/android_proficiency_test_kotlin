package com.antonystudio.androidproficiencttestkotlin.network

import com.antonystudio.androidproficiencttestkotlin.response.FailureResponse

interface ApiFailureHandlerInterface {
    fun onFailureResponse(response: FailureResponse?)
}