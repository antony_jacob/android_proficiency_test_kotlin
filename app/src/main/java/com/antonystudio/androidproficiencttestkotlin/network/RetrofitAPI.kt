package com.antonystudio.androidproficiencttestkotlin.network

import com.antonystudio.androidproficiencttestkotlin.response.Facts
import retrofit2.Call
import retrofit2.http.GET

interface RetrofitAPI {
    @GET("/s/2iodh4vg0eortkl/facts.json")
    fun getfactsList() : Call<Facts>
}