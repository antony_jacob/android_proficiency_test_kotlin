package com.antonystudio.androidproficiencttestkotlin.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.antonystudio.androidproficiencttestkotlin.R
import com.antonystudio.androidproficiencttestkotlin.fragment.FactFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val factFragment = FactFragment()
        val fragmentTransaction =
            supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(
            android.R.anim.fade_in,
            android.R.anim.fade_out
        )
        fragmentTransaction.replace(R.id.fragment_container, factFragment, "FactFragement")
        fragmentTransaction.commitAllowingStateLoss()
    }
}