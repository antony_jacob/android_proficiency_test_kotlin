package com.antonystudio.androidproficiencttestkotlin.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Rows(
    @field:SerializedName("title") var title: String,
    @field:SerializedName("description") var description: String,
    @field:SerializedName(   "imageHref"
    ) var imageHref: String
) :
    Serializable