package com.antonystudio.androidproficiencttestkotlin.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Facts : Serializable {
    @SerializedName("title")
    var title: String? = null

    @SerializedName("rows")
    var rows: List<Rows>? = null

}