package com.antonystudio.androidproficiencttestkotlin.response

import okhttp3.ResponseBody

class FailureResponse {
    var failureMessage: String? = null
    var responseBody: ResponseBody? = null
    var exception: Throwable? = null
}