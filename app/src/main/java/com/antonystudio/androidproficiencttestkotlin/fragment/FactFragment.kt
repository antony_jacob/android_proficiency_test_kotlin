package com.antonystudio.androidproficiencttestkotlin.fragment

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.antonystudio.androidproficiencttestkotlin.R
import com.antonystudio.androidproficiencttestkotlin.adapter.FactsAdapter
import com.antonystudio.androidproficiencttestkotlin.network.ApiFailureHandlerInterface
import com.antonystudio.androidproficiencttestkotlin.network.RetrofitAPI
import com.antonystudio.androidproficiencttestkotlin.network.RetrofitClientInstance
import com.antonystudio.androidproficiencttestkotlin.response.Facts
import com.antonystudio.androidproficiencttestkotlin.response.FailureResponse
import com.antonystudio.androidproficiencttestkotlin.utils.CommonUtils
import com.google.gson.Gson
import com.squareup.otto.Subscribe
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit


open class FactFragment : Fragment(), ApiFailureHandlerInterface {
    private lateinit var mSwipeRefreshLayout: SwipeRefreshLayout
    private var mProgressbar: ProgressBar? = null
    private var mListView: RecyclerView? = null
    private var errorTextView: TextView? = null
    private var dialog: AlertDialog? = null
    private var toolbar: Toolbar? = null
    private var mPreference: SharedPreferences? = null
    private var mActivity: Activity? = null
    private var retrofit: Retrofit? = RetrofitClientInstance.retrofitInstance
    private var retrofitAPI: RetrofitAPI = retrofit?.create<RetrofitAPI>(RetrofitAPI::class.java)!!
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        val view: View = inflater.inflate(R.layout.fragment_facts, container, false)
        mActivity = activity
        mSwipeRefreshLayout = view.findViewById(R.id.pull_to_refresh)
        mProgressbar = view.findViewById(R.id.progress_process)
        mListView = view.findViewById(R.id.listview_details)
        errorTextView = view.findViewById(R.id.error_msg);
        toolbar = view.findViewById(R.id.title_toolbar)
        dialog = activity?.let { CommonUtils.getAlert(it, "") }
        mPreference = mActivity!!.getSharedPreferences("USER", Context.MODE_PRIVATE)
        mSwipeRefreshLayout.setOnRefreshListener(){
            getData()
            mSwipeRefreshLayout.isRefreshing = false
        }
        getData()
        savedInstanceState?.putInt("saved_state",1)
        return view
    }

    //calling the service for api
    private fun getData() {
        if (!mActivity?.let { CommonUtils.isNetConnected(it) }!!) {
            showBadInternetConnectionAlert(mActivity)
            return
        }
        mProgressbar?.visibility = View.VISIBLE
        val apiInterface = retrofitAPI.getfactsList()
        apiInterface.enqueue( object : Callback<Facts> {
            override fun onResponse(call: Call<Facts>?, response: Response<Facts>?) {

                if (response != null) {
                    if (response.isSuccessful) {
                     //   response.body()?.let { bus?.post(it) }
                        response.body()?.let { factsAvailable(it) }
                    } else {
                        handleFailureResponse(null, response.errorBody())
                        mListView?.visibility = View.GONE
                        errorTextView?.visibility = View.VISIBLE
                        errorTextView?.text = getString(R.string.api_failure_message)
                    }
                }
            }

            override fun onFailure(call: Call<Facts>?, t: Throwable?) {
                handleFailureResponse(t, null)
                mListView?.visibility = View.GONE
                errorTextView?.visibility = View.VISIBLE
                errorTextView?.text = getString(R.string.api_failure_message)
            }
        })
    }

    private fun handleFailureResponse(throwable: Throwable?,responseBody: ResponseBody?) {

        val failureResponse = FailureResponse()
        failureResponse.failureMessage = getString(R.string.api_failure_message)
        failureResponse.responseBody = responseBody
        failureResponse.exception = throwable
    }

    //response from the api call
    fun factsAvailable(facts: Facts) {
        mProgressbar!!.visibility = View.GONE
        mListView?.visibility = View.VISIBLE
        errorTextView?.visibility = View.GONE
        val editor = mPreference!!.edit()
        val gson = Gson()
        val mgson = gson.toJson(facts)
        editor.putString("factdetails", mgson)
        editor.apply()
        toolbar?.title = facts.title
        if (facts.rows?.size!= 0) run {
            val adapter: FactsAdapter? = mActivity?.let { FactsAdapter(it) }
            facts.rows?.let {
                adapter?.setRowList(it)
            }
            mListView!!.setHasFixedSize(true)
            val mLayoutManager = LinearLayoutManager(mActivity)
            mListView!!.layoutManager = mLayoutManager
            mListView!!.adapter = adapter
            adapter?.notifyDataSetChanged()
        }
    }

    private fun showBadInternetConnectionAlert(mainActivity: Activity?) {
        dialog!!.setMessage(mainActivity!!.getString(R.string.bad_internet_connection))
        if (!dialog!!.isShowing) dialog!!.show()
        mListView?.visibility = View.GONE
        errorTextView?.visibility = View.VISIBLE
        errorTextView?.text = getString(R.string.bad_internet_connection)
        val mJson = mPreference!!.getString("factdetails", "")!!
        if (mJson != "") {
            val gson = Gson()
            val mFacts: Facts = gson.fromJson<Facts>(mJson, Facts::class.java)
            factsAvailable(mFacts)
        }

    }

    override fun onFailureResponse(response: FailureResponse?) {
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        val mPos: Int? = savedInstanceState?.getInt("saved_state",0)
        if (mPos !=0) {
            val mJson = mPreference!!.getString("factdetails", "")!!
            if (mJson != "") {
                val gson = Gson()
                val mFacts: Facts = gson.fromJson<Facts>(mJson, Facts::class.java)
                factsAvailable(mFacts)
            } else {
                getData()
            }
        }
    }

}


